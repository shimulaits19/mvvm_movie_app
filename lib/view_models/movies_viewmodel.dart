
import 'package:omdb_movies/models/Movie.dart';

class MovieViewModel {

  final Movie movie;

  MovieViewModel({this.movie});

  String get title {
    return this.movie.title;
  }

  String get poster {
    return this.movie.poster;
  }

  String get year {
    return this.movie.year;
  }

}