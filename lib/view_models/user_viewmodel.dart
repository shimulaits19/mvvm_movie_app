import 'package:flutter/material.dart';
import 'package:omdb_movies/models/RpUser.dart';
import 'package:omdb_movies/services/webservices.dart';
import 'package:omdb_movies/utils/base_models.dart';

class UserViewModel extends BaseModel{
  RpUser user = RpUser();
  Future<void> createUser(var body) async {
    setState(ViewState.Busy);
    notifyListeners();
    user =  await WebServices().createUser(body);
    setState(ViewState.Idle);
    notifyListeners();
  }
}