import 'package:omdb_movies/utils/base_models.dart';
import 'package:flutter/material.dart';
import 'package:omdb_movies/services/webservices.dart';
import 'package:omdb_movies/view_models/movies_viewmodel.dart';

class MovieListViewModel extends BaseModel {

  List<MovieViewModel> movies = List<MovieViewModel>();

  Future<void> fetchMovies(String keyword) async {
    setState(ViewState.Busy);
    notifyListeners();
    final results =  await WebServices().fetchMovies(keyword);
    this.movies = results.map((item) => MovieViewModel(movie: item)).toList();
    setState(ViewState.Idle);
    notifyListeners();
  }

}