import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:omdb_movies/pages/movie_details_page.dart';
import 'package:omdb_movies/view_models/movies_viewmodel.dart';

class MovieList extends StatelessWidget {

  final List<MovieViewModel> movies;

  MovieList({this.movies});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: this.movies.length,
      itemBuilder: (context, index) {

        final movie = this.movies[index];

        return InkWell(
          onTap: (){
            Get.to(MovieDetailsPage(movie: movies[index],), transition: Transition.zoom, duration: Duration(seconds: 1));
          },
          child: Card(
            child: ListTile(
              contentPadding: EdgeInsets.all(10),
              leading: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(movie.poster)
                    ),
                    borderRadius: BorderRadius.circular(6)
                ),
                width: 50,
                height: 100,
              ),
              title: Text(movie.title),
              subtitle: Text(movie.year),
            ),
          ),
        );
      },
    );
  }
}