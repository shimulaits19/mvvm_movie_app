import 'package:flutter/material.dart';
import 'package:omdb_movies/utils/base_models.dart';
import 'package:omdb_movies/view_models/movie_list_view_model.dart';
import 'package:omdb_movies/widgets/movie_list.dart';
import 'package:provider/provider.dart';

class MovieListPage extends StatefulWidget {
  @override
  _MovieListPageState createState() => _MovieListPageState();
}

class _MovieListPageState extends State<MovieListPage> {

  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      context.read<MovieListViewModel>().fetchMovies("batman");
    });


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Movies")
        ),
        body: Consumer<MovieListViewModel>(
          builder: (context, vm, child){
            return Container(
                padding: EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(.8),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    child: TextField(
                      controller: _controller,
                      onSubmitted: (value) {
                        if(value.isNotEmpty) {
                          vm.fetchMovies(value);
                          _controller.clear();
                        }
                      },
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                          hintText: "Search",
                          hintStyle: TextStyle(color: Colors.white),
                          border: InputBorder.none
                      ),

                    ),
                  ),
                  SizedBox(height: 12,),
                  Expanded(
                      child: vm.state == ViewState.Busy ? Center(child: CircularProgressIndicator(),) : MovieList(movies: vm.movies))
                ])
            );
          },
        ),

    );
  }
}