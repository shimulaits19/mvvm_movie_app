import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:omdb_movies/models/Movie.dart';
import 'package:omdb_movies/pages/create_user_page.dart';
import 'package:omdb_movies/view_models/movies_viewmodel.dart';



class MovieDetailsPage extends StatefulWidget {
  MovieViewModel movie;
  MovieDetailsPage({this.movie});
  @override
  _MovieDetailsPageState createState() => _MovieDetailsPageState();
}

class _MovieDetailsPageState extends State<MovieDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.movie.title}"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Card(
              child: Column(
                children: <Widget>[
                  Container(
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            RaisedButton(
                              onPressed: (){
                                Get.snackbar("Easy snackbar", "So easy man!", snackPosition: SnackPosition.BOTTOM);
                              },
                              color: Colors.greenAccent.withOpacity(.6),
                              child: Text("Snackbar"),
                            ),
                            RaisedButton(
                              onPressed: (){
                                Get.defaultDialog(title: "Oh man!", middleText: "So easy");
                              },
                              color: Colors.yellowAccent.withOpacity(.6),
                              child: Text("Dialog"),
                            ),
                            RaisedButton(
                              onPressed: (){
                                Get.changeTheme(Get.isDarkMode? ThemeData.light(): ThemeData.dark());

                              },
                              color: Colors.redAccent.withOpacity(.6),
                              child: Text("Theme"),
                            ),

                            RaisedButton(
                              onPressed: (){
                                Get.bottomSheet(
                                  Container(
                                    child: Wrap(
                                      children: <Widget>[
                                        ListTile(
                                            leading: Icon(Icons.music_note),
                                            title: Text('Music'),
                                            onTap: () => {}),
                                        ListTile(
                                          leading: Icon(Icons.videocam),
                                          title: Text('Video'),
                                          onTap: () => {},
                                        ),
                                        SizedBox(
                                          height: 100,
                                        ),
                                      ],
                                    ),
                                  ),
                                  backgroundColor: Colors.white,
                                );
                              },
                              color: Colors.redAccent.withOpacity(.6),
                              child: Text("BSheet"),
                            ),
                          ],
                        ),
                      ),
                    height: 300,
                    width: double.infinity,
                  ),
                  RaisedButton(
                    color: Colors.greenAccent,
                    child: Text("Create user"),
                    onPressed: (){
                      Get.to(CreateUserPage());
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
