import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:omdb_movies/constants/constants.dart';
import 'package:omdb_movies/models/RqUser.dart';
import 'package:omdb_movies/utils/base_models.dart';
import 'package:omdb_movies/view_models/user_viewmodel.dart';
import 'package:provider/provider.dart';
class CreateUserPage extends StatefulWidget {
  @override
  _CreateUserPageState createState() => _CreateUserPageState();
}

class _CreateUserPageState extends State<CreateUserPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController jobController = TextEditingController();
  RqUser user = RqUser();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => UserViewModel(),
      child: Consumer<UserViewModel>(
        builder: (context, model, child){
          return Scaffold(
            appBar: AppBar(
              title: Text("Create user", style: TextStyle(color: Colors.white),),
            ),
            body: Form(
              key: _formKey,
              child: ListView(
                scrollDirection: Axis.vertical,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: nameController,
                      decoration: const InputDecoration(
                        icon: Icon(Icons.person),
                        hintText: 'What do people call you?',
                        labelText: 'Name *',
                      ),
                      onSaved: (String value){
                        user.name = value;
                      },
                      validator: (value){
                        if(value.length == 0){
                          return "Your name can't be empty";
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 8,),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: jobController,
                      decoration: const InputDecoration(
                        icon: Icon(Icons.work),
                        hintText: 'What is your job?',
                        labelText: 'Job *',
                      ),
                      onSaved: (String value){
                        print(value);
                        user.job = value;
                      },
                      validator: (value){
                        if(value.length == 0){
                          return "Your job can't be empty";
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: 8,),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      height: 45,
                      child: RaisedButton(
                        onPressed: ()async{
                          if(_formKey.currentState.validate()){
                            _formKey.currentState.save();
                            var body = json.encode(user);
                            var result = await model.createUser(body);
                            nameController.clear();
                            jobController.clear();
                          }
                        },
                        color: Theme.of(context).accentColor,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            model.state == ViewState.Busy ? CircularProgressIndicator( backgroundColor: Colors.white,valueColor: new AlwaysStoppedAnimation<Color>(Colors.black),) : Container(),
                            SizedBox(width: 20,),
                            Text("CREATE", style: TextStyle(color: Colors.white),),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 40,),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:model.user.name == null ? Center(child: Text("Account not created yet"),):   Card(
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 12,),
                            Center(child: Text("Account created successfully", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),)),
                            SizedBox(height: 20,),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text("Name: ${model.user.name}"),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text("Job: ${model.user.job}"),
                            ),
                            Align(
                                child: Text("${model.user.createdAt}", style: TextStyle(color: Colors.grey),),
                              alignment: Alignment.bottomRight,
                            ),
                            SizedBox(height: 10,),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },

      ),
    );
  }
}
