import 'dart:convert';
import 'package:omdb_movies/models/Movie.dart';
import 'package:omdb_movies/constants/constants.dart';
import 'package:http/http.dart' as http;
import 'package:omdb_movies/models/RpUser.dart';

class WebServices{
  Future<List<Movie>> fetchMovies(String keyword) async {
    final url = "http://www.omdbapi.com/?s=$keyword&apikey=$API_KEY";
    final response = await http.get(url);
    if(response.statusCode == 200) {
      final body = jsonDecode(response.body);
      final Iterable json = body["Search"];
      return json.map((movie) => Movie.fromJson(movie)).toList();

    } else {
      throw Exception("Unable to perform request!");
    }
  }

  Future<RpUser> createUser(var body) async {
    print(body);
    final url = "https://reqres.in/api/users";
    final response = await http.post(
      url,
      body: body,
      headers:
        { 'Content-type': 'application/json','Accept': 'application/json'});
    print(response.statusCode);
    print(response.body);
    if(response.statusCode == 201) {
      final responseBody = jsonDecode(response.body);
      return RpUser.fromJson(responseBody);
    } else {
      throw Exception("Unable to perform request!");
    }
  }
}