import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:omdb_movies/pages/movie_list_page.dart';
import 'package:omdb_movies/view_models/movie_list_view_model.dart';
import 'package:omdb_movies/view_models/user_viewmodel.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
        title: "Movies",
        home: MultiProvider(
          providers: [
            ChangeNotifierProvider<MovieListViewModel>(create: (_) => MovieListViewModel()),
            ChangeNotifierProvider<UserViewModel>(create: (_) => UserViewModel()),
          ],
          child: MovieListPage(),
        ),
    );
  }
}
