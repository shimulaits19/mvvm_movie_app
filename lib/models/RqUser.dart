// To parse this JSON data, do
//
//     final rqUser = rqUserFromJson(jsonString);

import 'dart:convert';

RqUser rqUserFromJson(String str) => RqUser.fromJson(json.decode(str));

String rqUserToJson(RqUser data) => json.encode(data.toJson());

class RqUser {
  RqUser({
    this.name,
    this.job,
  });

  String name;
  String job;

  factory RqUser.fromJson(Map<String, dynamic> json) => RqUser(
    name: json["name"] == null ? null : json["name"],
    job: json["job"] == null ? null : json["job"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "job": job == null ? null : job,
  };
}
