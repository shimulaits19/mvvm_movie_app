// To parse this JSON data, do
//
//     final rpUser = rpUserFromJson(jsonString);

import 'dart:convert';

RpUser rpUserFromJson(String str) => RpUser.fromJson(json.decode(str));

String rpUserToJson(RpUser data) => json.encode(data.toJson());

class RpUser {
  RpUser({
    this.name,
    this.job,
    this.id,
    this.createdAt,
  });

  String name;
  String job;
  String id;
  DateTime createdAt;

  factory RpUser.fromJson(Map<String, dynamic> json) => RpUser(
    name: json["name"] == null ? null : json["name"],
    job: json["job"] == null ? null : json["job"],
    id: json["id"] == null ? null : json["id"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "job": job == null ? null : job,
    "id": id == null ? null : id,
    "createdAt": createdAt == null ? null : createdAt.toIso8601String(),
  };
}
