class Movie {

  final String title;
  final String poster;
  final String year;

  Movie({this.title, this.poster, this.year});

  factory Movie.fromJson(Map<String, dynamic> json) {
    return Movie(
        title: json["Title"],
        poster: json["Poster"],
        year: json["Year"]
    );
  }

}